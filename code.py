import pandas as pd
import numpy as np
from sklearn.preprocessing import RobustScaler
import random
import keras
from keras import models
from keras import layers
from keras import Input
from keras.models import Model
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import seaborn as sns
# roc curve and auc score
from sklearn.datasets import make_classification
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from keras import regularizers


def build_model(dim):
    print(dim,"---------------------")
    model = models.Sequential()
    model.add(layers.Dense(16, activation='relu', input_shape=(dim,)))
    model.add(layers.Dense(16, activation='relu'))
    model.add(layers.Dense(1, activation='sigmoid'))
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    return model

def plot_roc_curve(Auc):

    fpr1, tpr1, thresholds1 = roc_curve(Auc[0][0], Auc[0][1])
    optimal_idx1 = np.argmax(tpr1 - fpr1)
    optimal_idx2 = np.argmin(np.sqrt((np.power((1-tpr1),2)) + (np.power((1-(1-fpr1)),2))))
    print("Roc1",thresholds1[optimal_idx1])
    print("Roc1",thresholds1[optimal_idx2])

    fpr2, tpr2, thresholds2 = roc_curve(Auc[1][0], Auc[1][1])
    optimal_idx1 = np.argmax(tpr2 - fpr2)
    optimal_idx2 = np.argmin(np.sqrt((np.power((1 - tpr2), 2)) + (np.power((1 - (1 - fpr2)), 2))))
    print("Roc2", thresholds2[optimal_idx1])
    print("Roc2", thresholds2[optimal_idx2])

    fpr3, tpr3, thresholds3 = roc_curve(Auc[2][0], Auc[2][1])
    optimal_idx1 = np.argmax(tpr3 - fpr3)
    optimal_idx2 = np.argmin(np.sqrt((np.power((1 - tpr3), 2)) + (np.power((1 - (1 - fpr3)), 2))))
    print("Roc3", thresholds3[optimal_idx1])
    print("Roc3", thresholds3[optimal_idx2])

    fpr4, tpr4, thresholds4 = roc_curve(Auc[3][0], Auc[3][1])
    optimal_idx1 = np.argmax(tpr4 - fpr4)
    optimal_idx2 = np.argmin(np.sqrt((np.power((1 - tpr4), 2)) + (np.power((1 - (1 - fpr4)), 2))))
    print("Roc4", thresholds4[optimal_idx1])
    print("Roc4", thresholds4[optimal_idx2])

    fpr5, tpr5, thresholds5 = roc_curve(Auc[4][0], Auc[4][1])
    optimal_idx1 = np.argmax(tpr5 - fpr5)
    optimal_idx2 = np.argmin(np.sqrt((np.power((1 - tpr5), 2)) + (np.power((1 - (1 - fpr5)), 2))))
    print("Roc5", thresholds5[optimal_idx1])
    print("Roc5", thresholds5[optimal_idx2])

    plt.plot(fpr1, tpr1, color='crimson', label='Model(1)')
    plt.plot(fpr2, tpr2, color='black', label='Model(2)')
    plt.plot(fpr3, tpr3, color='navy', label='Model(3)')
    plt.plot(fpr4, tpr4, color='lime', label='Model(4)')
    plt.plot(fpr5, tpr5, color='orange', label='Model(5)')


    plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
    plt.xlabel('1 - Specificity')
    plt.ylabel('Sensitivity')
    plt.title('Receiver Operating Characteristic (ROC) Curve')
    plt.legend()
    plt.show()
    plt.clf()

    fig, ax = plt.subplots()
    sen = np.array(tpr1)
    spc = np.array(1-fpr1)
    thr = np.array(thresholds1)
    df = pd.DataFrame({'cut-off point': thr[1:],
                       'Sensitivity': sen[1:],
                       'Specificity': spc[1:]})

    plt.plot('cut-off point', 'Sensitivity', data=df,  color='dodgerblue',
             linewidth=2, label="Sensitivity")
    plt.plot('cut-off point', 'Specificity', data=df, color='darkred',
             linewidth=2, label="Specificity")
    formatterY = FuncFormatter(lambda y, pos: '{0:g}'.format(y))
    ax.yaxis.set_major_formatter(formatterY)

    plt.title("All features")
    plt.xlabel("cut-off point", fontsize=15)
    plt.ylabel("performance", fontsize=15)
    ax.tick_params(direction='out', labelsize=16, length=2, width=2, colors='black',
                   grid_color='blue', grid_alpha=0.5)

    plt.tight_layout()
    plt.legend()
    plt.show()

    plt.clf()
    fig, ax = plt.subplots()
    sen = np.array(tpr2)
    spc = np.array(1 - fpr2)
    thr = np.array(thresholds2)
    df = pd.DataFrame({'cut-off point': thr[1:],  # --accuracy pol
                       'Sensitivity': sen[1:],
                       'Specificity': spc[1:]})
    plt.plot('cut-off point', 'Sensitivity', data=df, color='dodgerblue',
             linewidth=2, label="Sensitivity")
    plt.plot('cut-off point', 'Specificity', data=df, color='darkred',
             linewidth=2, label="Specificity")
    formatterY = FuncFormatter(lambda y, pos: '{0:g}'.format(y))
    ax.yaxis.set_major_formatter(formatterY)
    plt.title("All features except DMBT1")
    plt.xlabel("cut-off point", fontsize=15)
    plt.ylabel("performance", fontsize=15)
    ax.tick_params(direction='out', labelsize=16, length=2, width=2, colors='black',
                   grid_color='blue', grid_alpha=0.5)
    plt.tight_layout()
    plt.legend()
    plt.show()

    plt.clf()

    fig, ax = plt.subplots()
    sen = np.array(tpr3)
    spc = np.array(1 - fpr3)
    thr = np.array(thresholds3)
    df = pd.DataFrame({'cut-off point': thr[1:],  # --accuracy pol
                       'Sensitivity': sen[1:],
                       'Specificity': spc[1:]})

    plt.plot('cut-off point', 'Sensitivity', data=df, color='dodgerblue',
             linewidth=2, label="Sensitivity")
    plt.plot('cut-off point', 'Specificity', data=df, color='darkred',
             linewidth=2, label="Specificity")
    formatterY = FuncFormatter(lambda y, pos: '{0:g}'.format(y))
    ax.yaxis.set_major_formatter(formatterY)
    plt.title("All features except CSTB")
    plt.xlabel("cut-off point", fontsize=15)
    plt.ylabel("performance", fontsize=15)
    ax.tick_params(direction='out', labelsize=16, length=2, width=2, colors='black',
                   grid_color='blue', grid_alpha=0.5)
    plt.tight_layout()
    plt.legend()
    plt.show()

    plt.clf()

    fig, ax = plt.subplots()
    sen = np.array(tpr4)
    spc = np.array(1 - fpr4)
    thr = np.array(thresholds4)
    df = pd.DataFrame({'cut-off point': thr[1:],  # --accuracy pol
                       'Sensitivity': sen[1:],
                       'Specificity': spc[1:]})

    plt.plot('cut-off point', 'Sensitivity', data=df, color='dodgerblue',
             linewidth=2, label="Sensitivity")
    plt.plot('cut-off point', 'Specificity', data=df, color='darkred',
             linewidth=2, label="Specificity")
    formatterY = FuncFormatter(lambda y, pos: '{0:g}'.format(y))
    ax.yaxis.set_major_formatter(formatterY)
    plt.title("All features except DMBT1, CSTB")
    plt.xlabel("cut-off point", fontsize=15)
    plt.ylabel("performance", fontsize=15)
    ax.tick_params(direction='out', labelsize=16, length=2, width=2, colors='black',
                   grid_color='blue', grid_alpha=0.5)
    plt.tight_layout()
    plt.legend()
    plt.show()

    plt.clf()

    fig, ax = plt.subplots()
    sen = np.array(tpr5)
    spc = np.array(1 - fpr5)
    thr = np.array(thresholds5)
    df = pd.DataFrame({'cut-off point': thr[1:],  # --accuracy pol
                       'Sensitivity': sen[1:],
                       'Specificity': spc[1:]})

    plt.plot('cut-off point', 'Sensitivity', data=df, color='dodgerblue',
             linewidth=2, label="Sensitivity")
    plt.plot('cut-off point', 'Specificity', data=df, color='darkred',
             linewidth=2, label="Specificity")
    formatterY = FuncFormatter(lambda y, pos: '{0:g}'.format(y))
    ax.yaxis.set_major_formatter(formatterY)
    plt.title("only Concentration_CSTB and Concentration_DMBT1")
    plt.xlabel("cut-off point", fontsize=15)
    plt.ylabel("performance", fontsize=15)
    ax.tick_params(direction='out', labelsize=16, length=2, width=2, colors='black',
                   grid_color='blue', grid_alpha=0.5)
    plt.tight_layout()
    plt.legend()
    plt.show()
    # plt.plot(tpr1, color='crimson', label='Sensitivity')
    # plt.plot(1-fpr1, color='blue', label='Specificity')
    # plt.xlabel('Cut-off points')
    # plt.ylabel('Value of performance')
    # plt.title('All features')
    # plt.legend()
    # plt.show()
    #
    # plt.clf()
    #
    # plt.plot(tpr2, color='crimson', label='Sensitivity')
    # plt.plot(1 - fpr2, color='blue', label='Specificity')
    # plt.xlabel('Cut-off points')
    # plt.ylabel('Value of performance')
    # plt.title('All features except DMBT1')
    # plt.legend()
    # plt.show()
    #
    # plt.clf()
    #
    # plt.plot(tpr3, color='crimson', label='Sensitivity')
    # plt.plot(1 - fpr3, color='blue', label='Specificity')
    # plt.xlabel('Cut-off points')
    # plt.ylabel('Value of performance')
    # plt.title('All features except CSTB')
    # plt.legend()
    # plt.show()
    #
    # plt.clf()
    #
    # plt.plot(tpr4, color='crimson', label='Sensitivity')
    # plt.plot(1 - fpr4, color='blue', label='Specificity')
    # plt.xlabel('Cut-off points')
    # plt.ylabel('Value of performance')
    # plt.title('All features except DMBT1 and CSTB')
    # plt.legend()
    # plt.show()


def create_dataset(gf, gt):
    Xg, ys = [], []
    lst = gf.shape[0] + gt.shape[0]
    f_counter = 0
    t_counter = 0
    for i in range(0, lst):
        rnd = random.randint(0, 100)
        if (rnd >= 50 and f_counter < gf.shape[0]) or (rnd < 50 and t_counter >= gt.shape[0]):
            z = gf.iloc[f_counter, :]
            Xg.append(z)
            ys.append(1)
            f_counter += 1

        elif (rnd < 50 and t_counter < gt.shape[0]) or (rnd >= 50 and f_counter >= gf.shape[0]):
            z = gt.iloc[t_counter, :]
            Xg.append(z)
            ys.append(0)
            t_counter += 1

    return np.array(Xg), np.array(ys)


pdf_total = []
cdf_total = []

control_df = pd.read_csv('C:\data\control.csv')
patient_df = pd.read_csv('C:\data\patient.csv')

#--------------------(1)------------------------
c_df = control_df.iloc[:, 1:]
cdf_total.append(c_df)
p_df = patient_df.iloc[:, 1:]
pdf_total.append(p_df)
#----------------------(2)---------------------
c_temp1 = control_df.iloc[:, 1]
c_temp2 = control_df.iloc[:, 4:]
c_df1 = pd.concat([c_temp1, c_temp2, ], axis=1)
cdf_total.append(c_df1)
p_temp1 = patient_df.iloc[:, 1]
p_temp2 = patient_df.iloc[:, 4:]
p_df1 = pd.concat([p_temp1, p_temp2, ], axis=1)
pdf_total.append(p_df1)
#--------------------------------------(3)-------------
c_df2 = control_df.iloc[:, 3:]
cdf_total.append(c_df2)
p_df2 = patient_df.iloc[:, 3:]
pdf_total.append(p_df2)
#-------------------------------------(4)---------------
c_df3 = control_df.iloc[:, 4:]
cdf_total.append(c_df3)
p_df3 = patient_df.iloc[:, 4:]
pdf_total.append(p_df3)
#--------------------------------------(5)------------------
c_df4 = control_df.iloc[:, 1:4]
cdf_total.append(c_df4)
p_df4 = patient_df.iloc[:, 1:4]
pdf_total.append(p_df4)



g_scale_column=['Concentration_CSTB','alpha', 'Concentration_DMBT1','age','job','sex','ed','cardiovascular'
,'nervous','cigarettes','drugs','medicine','vegetables','fast_food','sour','s_fish','spicy'
,'sleep','salty', 'GERD', 'ulcers', 'DMFT']
g_scaler = RobustScaler()
g_scaler = g_scaler.fit(c_df[g_scale_column])
c_df.loc[:, g_scale_column] = g_scaler.transform(c_df[g_scale_column].to_numpy())
g_scaler=g_scaler.fit(p_df[g_scale_column])
p_df.loc[:, g_scale_column] = g_scaler.transform(p_df[g_scale_column].to_numpy())

g_scale_column=['Concentration_CSTB','age','job','sex','ed','cardiovascular'
,'nervous','cigarettes','drugs','medicine','vegetables','fast_food','sour','s_fish','spicy'
,'sleep','salty', 'GERD', 'ulcers', 'DMFT']
g_scaler = RobustScaler()
g_scaler = g_scaler.fit(c_df1[g_scale_column])
c_df1.loc[:, g_scale_column] = g_scaler.transform(c_df1[g_scale_column].to_numpy())
g_scaler=g_scaler.fit(p_df1[g_scale_column])
p_df1.loc[:, g_scale_column] = g_scaler.transform(p_df1[g_scale_column].to_numpy())


g_scale_column=['Concentration_DMBT1','age','job','sex','ed','cardiovascular'
,'nervous','cigarettes','drugs','medicine','vegetables','fast_food','sour','s_fish','spicy'
,'sleep','salty', 'GERD', 'ulcers', 'DMFT']
g_scaler = RobustScaler()
g_scaler = g_scaler.fit(c_df2[g_scale_column])
c_df2.loc[:, g_scale_column] = g_scaler.transform(c_df2[g_scale_column].to_numpy())
g_scaler=g_scaler.fit(p_df2[g_scale_column])
p_df2.loc[:, g_scale_column] = g_scaler.transform(p_df2[g_scale_column].to_numpy())

g_scale_column=['age','job','sex','ed','cardiovascular'
,'nervous','cigarettes','drugs','medicine','vegetables','fast_food','sour','s_fish','spicy'
,'sleep','salty', 'GERD', 'ulcers', 'DMFT']
g_scaler = RobustScaler()
g_scaler = g_scaler.fit(c_df3[g_scale_column])
c_df3.loc[:, g_scale_column] = g_scaler.transform(c_df3[g_scale_column].to_numpy())
g_scaler=g_scaler.fit(p_df3[g_scale_column])
p_df3.loc[:, g_scale_column] = g_scaler.transform(p_df3[g_scale_column].to_numpy())

g_scale_column=['Concentration_CSTB','alpha','Concentration_DMBT1']
g_scaler = RobustScaler()
g_scaler = g_scaler.fit(c_df4[g_scale_column])
c_df4.loc[:, g_scale_column] = g_scaler.transform(c_df4[g_scale_column].to_numpy())
g_scaler=g_scaler.fit(p_df4[g_scale_column])
p_df4.loc[:, g_scale_column] = g_scaler.transform(p_df4[g_scale_column].to_numpy())

X_total = []
y_total = []
count = 5
Auc = []

for num in range(count):
    Xg, y = create_dataset(pdf_total[num], cdf_total[num])
    X_total.append(Xg)
    y_total.append(y)

for cnt in range(count):

    print("start round --->", cnt)

    train_data = X_total[cnt][0:40, :]
    train_lable = y_total[cnt][0:40]
    k = 4
    num_val_samples = len(train_data) // k
    num_epochs = 100
    all_scores = []
    i = 0
    for i in range(k):
        print('processing fold #', i)
        val_data = train_data[i * num_val_samples: (i + 1) * num_val_samples]
        val_targets = train_lable[i * num_val_samples: (i + 1) * num_val_samples]
        partial_train_data = np.concatenate(
            [train_data[:i * num_val_samples],
             train_data[(i + 1) * num_val_samples:]],
            axis=0)
        partial_train_targets = np.concatenate(
            [train_lable[:i * num_val_samples],
             train_lable[(i + 1) * num_val_samples:]],
            axis=0)
        model = build_model(train_data.shape[1])
        history = model.fit(partial_train_data, partial_train_targets,
                            epochs=num_epochs, batch_size=10, validation_data=(val_data, val_targets), verbose=0)

    test_data = X_total[cnt][40:, :]
    test_lable = y_total[cnt][40:]
    preds = model.predict(test_data)
    auc = roc_auc_score(test_lable, preds)
    print('AUC: %.2f' % auc)
    tpl = (test_lable, preds)
    Auc.append(tpl)
    y_pred = np.where(preds <= .3, 0, 1)
    tn, fp, fn, tp = metrics.confusion_matrix(test_lable, y_pred).ravel()
    print(tn)
    print(fp)
    print(fn)
    print(tp)
    confusion_matrix = metrics.confusion_matrix(test_lable, y_pred)
    print(confusion_matrix)
    Specificity = tn / (tn + fp)
    Sensitivity = tp / (fn + tp)
    PPV = tp / (tp + fp)
    NPV = tn / (fn + tn)
    print(Specificity)
    print(Sensitivity)
    print(PPV)
    print(NPV)
    print(metrics.accuracy_score(test_lable, y_pred))
    print("end level --->", cnt)

plot_roc_curve(Auc)
